FROM openjdk:11-jre-slim

COPY target/config-service-0.0.1-SNAPSHOT.jar /config-service.jar

EXPOSE 8000

ENTRYPOINT ["java", "-jar", "/config-service.jar"]
